-- Selection des salles selon le nombre de prises en ordre croissant
SELECT s.nom_salle, b.nom_bat, CAST(s.caracteristiques_salle->>'prises_elec' AS INTEGER) AS prises_elec, 
CAST(s.caracteristiques_salle->>'prises_res' AS INTEGER) AS prises_res
FROM Salle AS s
INNER JOIN Batiment AS b ON s.fk_batiment = b.nom_bat
ORDER BY prises_elec ASC, prises_res ASC;

-- Les moyens informatiques qui ont un OS Linux et son responsable
SELECT m.serie_moyen, e.donnees->>'nom' AS nom_responsable, 
e.donnees->>'prenom' AS prenom_responsable, m.nom_moyen
FROM Moyen_informatique AS m
INNER JOIN Employe AS e ON m.fk_employe_resp = e.num_badge
WHERE m.caracteristiques_moy->>'OS' = 'Linux' OR m.caracteristiques_moy->>'OS' = 'linux';

-- Les employes responsables des projets avec le nom du projet, les dates et le mail du responsable
SELECT e.donnees->>'nom' AS nom_responsable, e.donnees->>'prenom' AS prenom_responsable, 
e.donnees->>'email' AS mail_responsable, pe.fk_projet AS sigle_proj, p.nom AS nom_proj, 
p.start_date, p.end_date
FROM Employe AS e
INNER JOIN Projet_employe AS pe ON e.num_badge = pe.fk_employe
JOIN Projet AS p ON pe.fk_projet = p.sigle_proj
WHERE pe.role = 'responsable' OR pe.role = 'Responsable';
