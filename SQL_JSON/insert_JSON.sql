INSERT INTO Batiment (nom_bat, localisation, caracteristiques_bat) VALUES
    ('A001', '-91.346, 156.457', '{"superficie": 600, "etages": 4}'),
    ('A002', '54.657, 109.358', '{"superficie": 841, "etages": 5}'),
    ('B001', '-110.136, 140.547', '{"superficie": 123, "etages": 8}'),
    ('B002', '87.658, -10.245', '{"superficie": 244, "etages": 6}');

INSERT INTO Plans_Batiment (id_plan, fk_batiment, etage, chemin_plan) VALUES
    (1, 'A001', 1, '/batiments/A001/plans/plan_1.pdf'),
    (2, 'A001', 2, '/batiments/A001/plans/plan_2.pdf'),
    (3, 'A001', 3, '/batiments/A001/plans/plan_3.pdf'),
    (4, 'A002', 1, '/batiments/A002/plans/plan_1.pdf'),
    (5, 'A002', 2, '/batiments/A002/plans/plan_2.pdf'),
    (6, 'A002', 3, '/batiments/A002/plans/plan_3.pdf'),
    (7, 'A002', 4, '/batiments/A002/plans/plan_4.pdf'),
    (8, 'B001', 1, '/batiments/B001/plans/plan_1.pdf'),
    (9, 'B001', 2, '/batiments/B001/plans/plan_2.pdf'),
    (10, 'B002', 1, '/batiments/B002/plans/plan_1.pdf');

INSERT INTO Salle (nom_salle, fk_batiment, etage, photos_plan, caracteristiques_salle) VALUES
    ('A_X1', 'A001', 2, '{"chemin_photos": "/salles/A_X1/photos", "chemin_plan": "/salles/A_X1/plans"}', '{"superficie": 21, "capacite_max": 5, "arrive_air": "TRUE", "arrive_triphase": "FALSE", "arrive_gaz": "o2", "prises_elec": 5, "prises_res": 3}'),
    ('A_X2', 'A001', 2, '{"chemin_photos": "/salles/A_X2/photos", "chemin_plan": "/salles/A_X2/plans"}', '{"superficie": 45, "capacite_max": 7, "arrive_air": "TRUE", "arrive_triphase": "FALSE", "prises_elec": 8, "prises_res": 5}'),
    ('A_X3', 'A001', 3, '{"chemin_photos": "/salles/A_X3/photos", "chemin_plan": "/salles/A_X3/plans"}', '{"superficie": 57, "capacite_max": 9, "arrive_air": "FALSE", "arrive_triphase": "TRUE", "arrive_gaz": "butane", "prises_elec": 15, "prises_res": 13}'),
    ('A_X4', 'A002', 2, '{"chemin_photos": "/salles/A_X4/photos", "chemin_plan": "/salles/A_X4/plans"}', '{"superficie": 24, "capacite_max": 10, "arrive_air": "FALSE", "arrive_triphase": "TRUE", "arrive_gaz": "metane", "prises_elec": 10, "prises_res": 9}'),
    ('A_X5', 'A002', 2, '{"chemin_photos": "/salles/A_X5/photos", "chemin_plan": "/salles/A_X5/plans"}', '{"superficie": 46, "capacite_max": 5, "arrive_air": "FALSE", "arrive_triphase": "TRUE", "prises_elec": 11, "prises_res": 13}'),
    ('A_X6', 'A002', 2, '{"chemin_photos": "/salles/A_X6/photos", "chemin_plan": "/salles/A_X6/plans"}', '{"superficie": 32, "capacite_max": 15, "arrive_air": "TRUE", "arrive_triphase": "FALSE", "arrive_gaz": "o2", "prises_elec": 19, "prises_res": 14}'),
    ('A_X7', 'A002', 3, '{"chemin_photos": "/salles/A_X7/photos", "chemin_plan": "/salles/A_X7/plans"}', '{"superficie": 46, "capacite_max": 6, "arrive_air": "FALSE", "arrive_triphase": "TRUE", "prises_elec": 5, "prises_res": 3}'),
    ('A_X8', 'A002', 3, '{"chemin_photos": "/salles/A_X8/photos", "chemin_plan": "/salles/A_X8/plans"}', '{"superficie": 79, "capacite_max": 3, "arrive_air": "TRUE", "arrive_triphase": "FALSE", "arrive_gaz": "azote", "prises_elec": 15, "prises_res": 17}'),
    ('A_X9', 'A002', 4, '{"chemin_photos": "/salles/A_X9/photos", "chemin_plan": "/salles/A_X9/plans"}', '{"superficie": 12, "capacite_max": 17, "arrive_air": "TRUE", "arrive_triphase": "FALSE", "prises_elec": 9, "prises_res": 13}'),
    ('B_X1', 'B001', 2, '{"chemin_photos": "/salles/B_X1/photos", "chemin_plan": "/salles/B_X1/plans"}', '{"superficie": 10, "capacite_max": 15, "arrive_air": "TRUE", "arrive_triphase": "TRUE", "prises_elec": 15, "prises_res": 8}'),
    ('B_X2', 'B001', 2, '{"chemin_photos": "/salles/B_X2/photos", "chemin_plan": "/salles/B_X2/plans"}', '{"superficie": 34, "capacite_max": 9, "arrive_air": "TRUE", "arrive_triphase": "FALSE", "arrive_gaz": "azote", "prises_elec": 18, "prises_res": 7}');

INSERT INTO Site_Industriel (sigle, nom) VALUES
    ('D_X001', 'Site Alpha'),
    ('D_X002', 'Site Bravo'),
    ('D_X003', 'Site Charlie'),
    ('L_X001', 'Site Delta'),
    ('L_X002', 'Site Echo'),
    ('L_X003', 'Site Foxtrot');
   
INSERT INTO Departement (sigle_dep, domain) VALUES
    ('D_X001', 'Microbiologie'),
    ('D_X002', 'Manipulation genetique'),
    ('D_X003', 'Explosives');

INSERT INTO Laboratoire (sigle_lab, chemin_logo, thematique) VALUES
    ('L_X001', '/sites_industriels/laboratoire/L_X001/logo.png', 'Armes chimiques'),
    ('L_X002', '/sites_industriels/laboratoire/L_X002/logo.png', 'Sante'),
    ('L_X003', '/sites_industriels/laboratoire/L_X003/logo.png', 'Extraction miniere');

INSERT INTO Employe (num_badge, fk_salle_bureau, fk_site, donnees, statut) VALUES
    (1001, 'B_X1', 'D_X001', '{"nom": "Lenoir", "prenom": "Alice", "email": "alice.lenoir@umbrella-corp.fr"}', 'CDI'),
    (1002, 'B_X1', 'D_X002', '{"nom": "Leblanc", "prenom": "Albert", "email": "albert.leblanc@umbrella-corp.fr"}', 'CDI'),
    (1003, 'B_X1', 'D_X003', '{"nom": "Lerouge", "prenom": "Jacques", "email": "jacques.lerouge@umbrella-corp.fr"}', 'CDI'),
    (1004, 'B_X2', 'L_X001', '{"nom": "Lebleu", "prenom": "Zoe", "email": "zoe.lebleu@umbrella-corp.fr"}', 'CDI'),
    (1005, 'B_X2', 'L_X002', '{"nom": "Levert", "prenom": "Marcel", "email": "marcel.levert@umbrella-corp.fr"}', 'CDI'),
    (1006, 'B_X2', 'L_X003', '{"nom": "Lerose", "prenom": "Leon", "email": "leon.lerose@umbrella-corp.fr"}', 'CDI'),
    (2010, NULL, 'D_X001', '{"nom": "Robert", "prenom": "Jean", "email": "jean.robert@umbrella-corp.fr"}', 'CDD'),
    (2011, NULL, 'D_X001', '{"nom": "Bruan", "prenom": "Axel", "email": "axel.bruan@umbrella-corp.fr"}', 'CDD'),
    (2012, NULL, 'L_X001', '{"nom": "Billot", "prenom": "Pauline", "email": "pauline.billot@umbrella-corp.fr"}', 'CDD'),
    (2013, NULL, 'L_X001', '{"nom": "Fredo", "prenom": "Paul", "email": "paul.fredo@umbrella-corp.fr"}', 'CDD'),
    (9001, NULL, NULL, '{"nom": "Durant", "prenom": "Pierre", "email": "pierre.durant@umbrella-corp.fr"}', 'stagiaire'),
    (9002, NULL, NULL, '{"nom": "Duval", "prenom": "Louis", "email": "louis.duval@umbrella-corp.fr"}', 'stagiaire');

UPDATE Site_Industriel
SET fk_directeur=1001
WHERE sigle='D_X001';

UPDATE Site_Industriel
SET fk_directeur=1002
WHERE sigle='D_X002';

UPDATE Site_Industriel
SET fk_directeur=1003
WHERE sigle='D_X003';

UPDATE Site_Industriel
SET fk_directeur=1004
WHERE sigle='L_X001';

UPDATE Site_Industriel
SET fk_directeur=1005
WHERE sigle='L_X002';

UPDATE Site_Industriel
SET fk_directeur=1006
WHERE sigle='L_X003';

INSERT INTO Poste_tel (num_int, fk_salle, fk_proprietaire, num_ext, type, caracteristiques_poste) VALUES
    (501, 'A_X1', 2010, 5540501, 'VOIP', '{"modele": "ARX01", "marque": "Motorola"}'),
    (502, 'A_X1', 2011, 5540502, 'VOIP', '{"modele": "ARX02", "marque": "Motorola"}'),
    (9001, 'A_X9', 2012, 55409001, 'TOIP', NULL),
    (9002, 'A_X9', 2013, 55409002, 'landline', '{"marque": "Cisco"}');

INSERT INTO Projet (sigle_proj, fk_site, nom, start_date, end_date, description) VALUES
    ('PR01', 'D_X001', 'Recherche du mais', '2018-09-28', '2020-01-10', 'Un labour de 25 à 30 cm suivi par un recroisement effectué avec un cultivateur pour travailler en profondeur et aérer la terre.'),
    ('PR02', 'D_X001', 'Vaccination', '2018-09-28', NULL, ' Les vaccins, qui stimulent le système immunitaire, prémunissent la personne d’une infection ou d’une maladie.'),
    ('PR03', 'L_X001', 'Bombe a hydrogene', '2020-05-28', NULL, NULL);

INSERT INTO Projet_Employe (id, fk_projet, fk_employe, role, commentaires) VALUES
    (101, 'PR01', 1001, 'Responsable', 'Directeur du Site Alpha.'),
    (102, 'PR01', 9001, 'Cultivateur', 'Stagiaire en developpement.'),
    (103, 'PR01', 9002, 'Ingenieur en software', 'Stagiaire en developpement.'),
    (104, 'PR02', 1002, 'Responsable', 'Directeur du Site Bravo.'),
    (105, 'PR02', 2010, 'Medecin', NULL),
    (106, 'PR02', 2011, 'Laboratoriste', NULL),
    (107, 'PR02', 2012, 'Laboratoriste', NULL),
    (108, 'PR02', 2013, 'Avocat', 'Pendant le proces du 07/01/2020'),
    (109, 'PR03', 1003, 'Responsable', 'Directeur du Site Charlie.'),
    (110, 'PR03', 2010, 'Chimique', NULL);

INSERT INTO Machine (serie_machine, fk_salle, modele, description, caracteristiques_mach, taille, machine_fabrication, machine_laboratoire) VALUES
    ('XEW9012', 'A_X4', 'SDT-ULTRA', 'Microscope', '{"puissance_elec": 120, "besoin_tri": "FALSE", "besoin_res": "FALSE", "num_maintenance": 465867, "entreprise_maint": "Schneider"}', 'petite', FALSE, TRUE),
    ('AB001', 'A_X4', 'Xtreme', 'Centrifugeuse', '{"puissance_elec": 240, "besoin_tri": "TRUE", "besoin_res": "FALSE", "num_maintenance": 68592, "entreprise_maint": "Epson", "besoin_gaz": "o2"}', 'mediane', FALSE, TRUE),
    ('XXX-12381', 'A_X5', 'Hadrone', 'Grand collisionneur de hadrons', '{"puissance_elec": 2780, "besoin_tri": "TRUE", "besoin_res": "TRUE", "num_maintenance": 101, "entreprise_maint": "TForce", "besoin_gaz": "azote"}', 'grande', FALSE, TRUE);

INSERT INTO Moyen_Informatique (serie_moyen, fk_machine_liee, fk_employe_resp, nom_moyen, caracteristiques_moy, serveur, pc, portable) VALUES
    ('EKQR0012', 'XEW9012', 1001, 'Super PC 1', '{"OS": "Windows", "mac_ethernet": "00:1e:c2:9e:28:6b"}', FALSE, TRUE, FALSE),
    ('EKQR00120', 'AB001', 1001, 'Super PC 2', '{"OS": "Linux"}', TRUE, FALSE, FALSE),
    ('EKQR00122', 'AB001', 1001, 'Portable HP', '{"OS": "Linux", "mac_wifi": "12:1f:c2:00:8e:6a", "mac_ethernet": "00:1f:c2:5b:8e:6a"}', FALSE, FALSE, TRUE),
    ('EWE721', 'XXX-12381', 1004, 'Hyper serveur', '{"OS": "Linux", "mac_wifi": "09:10:c2:d0:8d:1b", "mac_ethernet": "10:00:d5:3a:9c:3f"}', TRUE, FALSE, FALSE),
    ('CBWN1515', 'XXX-12381', 1005, 'Portable test', '{"OS": "Linux", "mac_wifi": "10:1d:d1:5b:9c:6a"}', FALSE, FALSE, TRUE);

INSERT INTO Moyen_Projet (id, fk_moyen, fk_projet, commentaires) VALUES
    (301, 'EKQR0012', 'PR01', 'Analise des molecules du mais'),
    (302, 'EKQR0012', 'PR02', 'Analise des cellules'),
    (303, 'EKQR00120', 'PR02', NULL),
    (304, 'EKQR00122', 'PR02', NULL),
    (305, 'EKQR0012', 'PR03', 'Analise des molecules de la bombe'),
    (306, 'EWE721', 'PR03', 'Traitement des donnees du collisionneur'),
    (307, 'CBWN1515', 'PR03', NULL);
