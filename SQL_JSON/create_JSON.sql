-- Creation des tables

CREATE TABLE Batiment (
	nom_bat VARCHAR(30) PRIMARY KEY,
	localisation VARCHAR(30) NOT NULL,
	caracteristiques_bat JSON
	);

CREATE TABLE Plans_Batiment (
	id_plan INT PRIMARY KEY,
	fk_batiment VARCHAR(30),
	etage INT NOT NULL,
	chemin_plan VARCHAR(50) NOT NULL,
	FOREIGN KEY (fk_batiment) REFERENCES Batiment(nom_bat)
	);

CREATE TABLE Salle (
	nom_salle VARCHAR(30) PRIMARY KEY,
	fk_batiment VARCHAR(30),
	etage INT NOT NULL,
	photos_plan JSON NOT NULL,
	caracteristiques_salle JSON,
	FOREIGN KEY (fk_batiment) REFERENCES Batiment(nom_bat)
	);

CREATE TABLE Site_Industriel (
	sigle VARCHAR(10) PRIMARY KEY,
	fk_directeur INT,
	nom VARCHAR(30) NOT NULL
	);

CREATE TABLE Departement (
	sigle_dep VARCHAR(10),
	domain VARCHAR(30) NOT NULL,
	FOREIGN KEY (sigle_dep) REFERENCES Site_Industriel(sigle)
	);

CREATE TABLE Laboratoire (
	sigle_lab VARCHAR(10),
	chemin_logo VARCHAR(50) NOT NULL,
	thematique VARCHAR(30) NOT NULL,
	FOREIGN KEY (sigle_lab) REFERENCES Site_Industriel(sigle)
	);

CREATE TABLE Employe (
	num_badge INT PRIMARY KEY,
	fk_salle_bureau VARCHAR(30),
	fk_site VARCHAR(10),
	donnees JSON NOT NULL,
	statut VARCHAR(30) CHECK(statut IN('CDD', 'CDI', 'stagiaire')),
	FOREIGN KEY (fk_salle_bureau) REFERENCES Salle(nom_salle),
	FOREIGN KEY (fk_site) REFERENCES Site_Industriel(sigle)
	);

CREATE TABLE Poste_tel (
	num_int INT PRIMARY KEY,
	fk_salle VARCHAR(30),
	fk_proprietaire INT,
	num_ext INT NOT NULL,
	type VARCHAR(30) CHECK(type IN('VOIP', 'TOIP', 'landline')) NOT NULL,
	caracteristiques_poste JSON,
	FOREIGN KEY (fk_salle) REFERENCES Salle(nom_salle),
	FOREIGN KEY (fk_proprietaire) REFERENCES Employe(num_badge)
	);

CREATE TABLE Projet (
	sigle_proj VARCHAR(10) PRIMARY KEY,
	fk_site VARCHAR(10),
	nom VARCHAR(30) NOT NULL,
	start_date DATE NOT NULL,
	end_date DATE,
	description TEXT,
	FOREIGN KEY (fk_site) REFERENCES Site_Industriel(sigle)
	);

CREATE TABLE Projet_Employe (
	id INT PRIMARY KEY,
	fk_projet VARCHAR(10),
	fk_employe INT,
	role VARCHAR(30) NOT NULL,
	commentaires TEXT,
	FOREIGN KEY (fk_projet) REFERENCES Projet(sigle_proj),
	FOREIGN KEY (fk_employe) REFERENCES Employe(num_badge)
	);

CREATE TABLE Machine (
	serie_machine VARCHAR(30) PRIMARY KEY,
	fk_salle VARCHAR(30),
	modele VARCHAR(30) NOT NULL,
	description TEXT,
	caracteristiques_mach JSON,
	taille VARCHAR(30) CHECK(taille IN('petite', 'mediane', 'grande')) NOT NULL,
	machine_fabrication BOOLEAN,
	machine_laboratoire BOOLEAN,
	FOREIGN KEY (fk_salle) REFERENCES Salle(nom_salle)
	);

CREATE TABLE Moyen_Informatique (
	serie_moyen VARCHAR(30) PRIMARY KEY,
	fk_machine_liee VARCHAR(30),
	fk_employe_resp INT,
	nom_moyen VARCHAR(30) NOT NULL,
	caracteristiques_moy JSON,
	serveur BOOLEAN,
	pc BOOLEAN,
	portable BOOLEAN,
	FOREIGN KEY (fk_machine_liee) REFERENCES Machine(serie_machine),
	FOREIGN KEY (fk_employe_resp) REFERENCES Employe(num_badge)
	);

CREATE TABLE Moyen_Projet (
	id INT PRIMARY KEY,
	fk_moyen VARCHAR(30),
	fk_projet VARCHAR(10),
	commentaires TEXT,
	FOREIGN KEY (fk_moyen) REFERENCES Moyen_Informatique(serie_moyen),
	FOREIGN KEY (fk_projet) REFERENCES Projet(sigle_proj)
	);

-- On ajoute la dernier clé étragère

ALTER TABLE Site_Industriel
ADD FOREIGN KEY (fk_directeur) REFERENCES Employe(num_badge);
