# Note de clarification
## Liste d'objets

### Bâtiment:
* nom (clé primaire)
* superficie
* localisation
* nombre d'étages


### Salle
* nom (clé primaire)
* superficie
* capacité maximale
* étage
* photos (chemin du fichier)
* plan (chemin du fichier)
* caractéristiques tecniques


### Plans des bâtiments
* id (clé primaire)
* étage
* plan (chemin du fichier)


### Employé
* numéro de badge (clé primaire)
* nom
* prénom
* email
* statut (CDI, CDD, Stagiaire)
* poste


### Machine
* numéro de serie (clé primaire)
* modèle
* description (peut être NULL)
* puissance électrique (peut être NULL)
* besoin de triphase ?
* besoin de réseau ?
* numéro de contrat de maintenance
* entreprise de maintenance
* besoin de gaz ?
* taille (petite, médiane, grande)


### Poste téléphonique
* numéro intereur (clé primaire)
* numéro exterieur
* type (VOIP, TOIP, Landline)
* modèle (peut être NULL)
* marque (peut être NULL)


### Moyenne informatique
* numéro de serie (clé primaire)
* nom
* OS
* adresse MAC Wi-Fi (peut être NULL)
* adresse MAC ethernet (peut être NULL)


### Laboratoire
* sigle (clé primaire)
* nom
* logo (chemin du fichier)
* thématique


### Département
* sigle (clé primaire)
* nom
* domain


### Projet
* sigle (clé primaire)
* nom
* start_date
* end_date (peut être NULL)
* description (peut être NULL)


## Associations
- Un bâtiment a plusieurs plans pour chaque étage (au moins 1), mais un plan n'appartient qu'un seul étage d'un bâtiment. Alors **1 --- 1...n**
- Une salle est dans un bâtiment. Si un bâtiment est en train d'être construit, il n'a aucune salle. Alors **1 --- 0...n**
- Une machine est dans une salle; une machine neuf peut être non-installée dans une salle et une salle peut n'avoir aucune machine. Alors **0...1 --- 0...n**
- Une poste téléphonique est dans une salle. La salle peut n'avoir aucune poste, et une poste appartient à une seule salle. Alors **1 --- 0...n**
- L'emplacement du bureau d'un employé se trouve dans une salle. Une salle peut avoir divers bureaux, et un employé ne peut avoir qu'un seul bureau (ou aucun bureau). Alors **0...1 --- 0...n**
- Une machine est liée à un ou plusieurs moyens informatiques, mais un moyen informatique ne peut être lié qu'à une seule machine (ou à aucune machine). Alors **0...1 --- 0...n**
- Un employé peut posséder un poste téléphonique (ou aucun) et un poste téléphonique est propriété au maximum d'un seul employé. Alors **0...1 --- 0...1**
- Un employé peut être membre ou directeur (ou pas) d'un seul site industriel, soit "Département" ou "Laboratoire". Puis, un site industriel admet plusierus membres et un seul directeur. Alors: 
    - **0...n --- 0...1** pour "est membre"
    - **1 --- 0...1** pour "est directeur"
- Un employé peut être responsable de plusieurs moyens informatiques (ou aucun), et un moyen informatique a qu'un seul responsable. Alors **1 --- 0...n**
- Un employé réalise plusieurs projets, et un projet est réalisé au minimum par un employé. Alors **0...n --- 1...n**
- Un projet peut utiliser plusieurs moyens informatiques pour le développement. Un moyen informatique peut être utilisé pour beaucoup de projets. Alors **0...n --- 0...n**
- Un site industriel a des projets en réalisation, mais un projet appartient à un seul site industriel. Alors **0...n --- 1**

## Hypothèses
- Un bâtiment peut n'avoir aucune salle.
- Un employé peut n'avoir aucun bureau.
- Une salle peut être vide (sans bureaux ni machines).
- Une machine peut n'être dans aucune salle (par exemple, quand elle est neuf et non installée sur place).
- Les postes téléphoniques ont un numéro interne et pour le numéro exterieur, on ajoute "5540" au début.
- Les postes téléphoniques peuvent (ou pas) appartenir à un employé.
- Les employés peuvent avoir au maximum une poste téléphonique.
- Les adresses MAC peuvent être nulles.

## Contraintes (mieux exprimées sur MLD.md)
On a une contrainte d'association 0...n - 1...n pour les classes Employé - Projet (réalise). Pour l'association 0...n - 0...n, il n'y a pas de contraintes d'existence simultanée de tuples.

Ensuite, il y a des contraintes pour le héritage par référence et par classe mère.

## Héritages (contraintes exprimées sur MLD.md)
### Par classe mère: Les classes filles ne sont pas répresentées par des relations, seule la classe mère (la clé primaire de la classe mère est utilisée pour identifier la relation):
- Machine <- (Machine_Fabrication, Machine_Laboratoire)
- Moyen_Informatique <- (Serveur, PC, Portable)

En plus, on a un héritage complet parce que les classes filles ne définissent pas d'attributs autre que ceux hérités.

### Par référence: Chaque classe est représentée par une relation et la clé primaire de la calsse mère est utilisé pour identifier chacune de ses classes filles:
- Site_Industriel <- (Département, Laboratoire)

En plus, on a un héritage exclusif parce que les objets d'une classe fille ne appartiennent aussi à une autre classe fille.

## Droits d'utilisateur (sur /SQL_Relationnel/droits.sql)
- SuperAdmin: tous les droits
- Directeur d'un site industriel:
    - Gestion des sites.
    - Gestion des projets.
    - Gestion des employés dans les projets.
- Admin RH:
    - Gestion des employés.
- Admin des bâtiments:
    - Gestion des bâtiments.
    - Gestion des salles.
    - Gestion des plans.
- Admin des ressources:
    - Gestion des machines.
    - Gestion des postes téléphoniques.
    - Gestion des moyens informatiques.
- Employé: aucun droit. Seul son registre est effectué.

## Vues (sur /SQL_Relationnel/vues.sql)
### Héritage
- vServeur: Montre les moyens informatiques qui sont des Serveurs.
- vPC: Montre les moyens informatiques qui sont des PC.
- vPortable: Montre les moyens informatiques qui sont des Portables.
- vMachine_Fabrication: Montre les machines qui sont de Fabrication.
- vMachine_Laboratoire: Montre les machines qui sont de Laboratoire.

### Autres vues
- vEmployes_Actifs: Les personnes qui sont actuellement dans un projet.
- vResponsables_Moyen: Les personnes qui sont responsables des moyens informatiques.
- vResponsables_Postes: Les personnes qui sont responsables des postes téléfoniques.
- vDirecteurs: Tous les employés qui sont directeurs des sites industriels.

## Requetes (sur /SQL_Relationnel/select.sql et /SQL_JSON/select.sql)
- Les employés qui ne sont actuellement dans aucun projet.
- Les salles qui ont une arrivé d'air, arrivé électrique triphasée ou un type de gaz.
- Plans des bâtiments.
- Les projets qui sont terminés.
- Selection des salles selon le nombre de prises éléctriques et réseau.
- Les moyens informatiques qui ont un OS Linux et son responsable.
- Les employés responsables des projets avec le nom du projet, les dates et le mail du responsable.
