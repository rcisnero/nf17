# Projet 2 - NF17

**Sujet 25 - Gestion du patrimoine**

[![SQL](https://img.shields.io/badge/-SQL-success.svg)](https://gitlab.utc.fr/rcisnero/nf17)
[![UML](https://img.shields.io/badge/-UML-important.svg)](https://gitlab.utc.fr/rcisnero/nf17)
[![JSON](https://img.shields.io/badge/-JSON-critical.svg)](https://gitlab.utc.fr/rcisnero/nf17)
