## Tables avec leurs attributs

Batiment(#nom_bat: varchar, superficie: int, localisation: varchar, nombre_etages: int)

Plans_Batiment(#id_plan: int, fk_batiment=>Batiment, etage: int, chemin_plan: varchar)

Salle(#nom_salle: varchar, fk_batiment=>Batiment, superficie: int, capacite_max: int, etage: int, chemin_photos: varchar, chemin_plan: varchar, caracteristiques_tec: Caracteristiques_tec)

Employe(#num_badge: int, fk_salle_bureau=>Salle, fk_site=>Site_Industriel, nom: varchar, prenom: varchar, email: varchar, statut: {CDI, CDD, stagiaire})

Site_Industriel(#sigle: varchar, fk_directeur=>Employe, nom: varchar)

Departement(#sigle_dep=>Site_Industriel, domain: varchar)

Laboratoire(#sigle_lab=>Site_Industriel, chemin_logo: varchar, thematique: varchar)

Poste_tel(#num_int: int, fk_salle=>Salle, fk_proprietaire=>Employe, num_ext: int, type: {VOIP, TOIP, landline}, modele: varchar, marque: varchar)

Projet(#sigle_proj: varchar, fk_site=>Site_Industriel, nom: varchar, start_date: date, end_date: date, description: text)

Projet_Employe(#id: int, fk_projet=>Projet, fk_employe=>Employe, role: varchar, commentaires: text)

Machine(#serie_machine: varchar, fk_salle=>Salle, modele: varchar, description: text, puissance_elec: int, besoin_tri: boolean, besoin_res: boolean, num_maintenance: int, entreprise_maintenance: varchar, besoin_gaz: Gaz, taille: {petite, mediane, grande}, t:{machine_fabrication, machine_laboratoire}: boolean) avec t non null

Moyen_Informatique(#serie_moyen: varchar, fk_machine_liee=>Machine, fk_employe_resp=>Employe, nom_moyen: varchar, OS: varchar, mac_wifi: varchar, mac_ethernet: varchar, t: {serveur, pc, portable}: boolean) avec t non null

Moyen_Projet(#id: int, fk_moyen=>Moyen_Informatique, fk_projet=>Projet, commentaires: text)


## Contraintes
### Association 0...n - 1...n pour **Employé - Projet**:
- PROJECTION(Projet, sigle_proj) = PROJECTION(Projet-Employe, sigle_proj)

### Héritage par référence:
##### Site_Industriel.
- vDepartement = JOINTURE(Site_Industriel, Departement, sigle = sigle_dep)
- vLaboratoire = JOINTURE(Site_Industriel, Laboratoire, sigle = sigle_lab)
##### Héritage exclusif.
- PROJECTION(Departement, sigle_dep) INTER PROJECTION(Laboratoire, sigle_lab) = {}

### Héritage par classe mère:
##### Machine.
- vMachine_Fabrication = PROJECTION(RESTRICTION(Machine, t = Machine_Fabrication), modele_mach, description, puissance_elec, besoin_tri, besoin_res, num_maintenance, entreprise_maintenance, besoin_gaz, taille)
- vMachine_Laboratoire = PROJECTION(RESTRICTION(Machine, t = Machine_Laboratoire), modele_mach, description, puissance_elec, besoin_tri, besoin_res, num_maintenance, entreprise_maintenance, besoin_gaz, taille)

