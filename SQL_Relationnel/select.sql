-- Employes qui ne sont actuellement dans aucun projet
SELECT * FROM employe 
WHERE num_badge NOT IN(SELECT num_badge from vEmployes_actifs);

-- Salles qui ont une arrive d'air
SELECT s.nom_salle, b.nom_bat, s.etage, b.localisation AS localisation_batiment
FROM Salle AS s
INNER JOIN Batiment AS b ON s.fk_batiment = b.nom_bat
WHERE (Caracteristiques_tec).arrive_air IS TRUE;

-- Salles qui ont une arrive electrique triphase
SELECT s.nom_salle, b.nom_bat, s.etage, b.localisation AS localisation_batiment
FROM Salle AS s
INNER JOIN Batiment AS b ON s.fk_batiment = b.nom_bat
WHERE (Caracteristiques_tec).arrive_elec_tri IS TRUE;

-- Salles qui ont une arrive gaz "butane" (ou quelque autre)
SELECT s.nom_salle, b.nom_bat, s.etage, b.localisation AS localisation_batiment
FROM Salle AS s
INNER JOIN Batiment AS b ON s.fk_batiment = b.nom_bat
WHERE (Caracteristiques_tec).arrive_gaz = 'butane';

-- Tous les plans des batiments
SELECT b.nom_bat, p.etage, p.chemin_plan
FROM Batiment AS b
INNER JOIN Plans_Batiment AS p ON b.nom_bat = p.fk_batiment;

-- Les projets qui sont terminées
SELECT p.sigle_proj, p.nom AS nom_projet, s.nom AS nom_site, p.start_date, p.end_date, p.description
FROM Projet AS p
INNER JOIN Site_industriel AS s ON p.fk_site = s.sigle
WHERE p.end_date IS NOT NULL;