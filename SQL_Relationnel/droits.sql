-- Creation des roles
CREATE ROLE SuperAdmin;
CREATE ROLE Directeur;
CREATE ROLE Admin_RH;
CREATE ROLE Admin_Batiments;
CREATE ROLE Admin_Ressources;
CREATE ROLE Employe;

-- On donne des privileges
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO SuperAdmin;
GRANT SELECT, INSERT, UPDATE ON Site_Industriel, Departement, Laboratoire, Projet, Projet_employe, Moyen_projet TO Directeur;
GRANT SELECT, INSERT, UPDATE ON Employe TO Admin_RH;
GRANT SELECT, INSERT, UPDATE ON Batiment, Plans_batiment, Salles TO Admin_Batiments;
GRANT SELECT, INSERT, UPDATE ON Poste_tel, Machine, Moyen_informatique TO Admin_Ressources;
REVOKE ALL ON ALL TABLES IN SCHEMA public FROM Employe;
