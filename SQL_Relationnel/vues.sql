CREATE VIEW vServeur AS
SELECT * FROM Moyen_Informatique
WHERE serveur = TRUE;

CREATE VIEW vPC AS
SELECT * FROM Moyen_Informatique
WHERE pc = TRUE;

CREATE VIEW vPortable AS
SELECT * FROM Moyen_Informatique
WHERE portable = TRUE;

CREATE VIEW vMachine_Fabrication AS
SELECT * FROM Machine
WHERE machine_fabrication = TRUE;

CREATE VIEW vMachine_Laboratoire AS
SELECT * FROM Machine
WHERE machine_laboratoire = TRUE;

CREATE VIEW vEmployes_Actifs AS
SELECT e.num_badge, e.nom, e.prenom, p.nom AS nom_projet, pe.role, pe.commentaires
FROM employe AS e
INNER JOIN projet_employe AS pe ON pe.fk_employe = e.num_badge
JOIN projet AS p ON p.sigle_proj = pe.fk_projet
WHERE p.end_date IS NULL;

CREATE VIEW vResponsables_Moyen AS
SELECT m.serie_moyen, m.nom_moyen, e.num_badge, e.nom, e.prenom
FROM moyen_informatique AS m
INNER JOIN employe AS e ON m.fk_employe_resp = e.num_badge;

CREATE VIEW vResponsables_Postes AS
SELECT p.num_int, p.type, p.modele, p.marque, e.num_badge, e.nom, e.prenom
FROM Poste_tel AS p
INNER JOIN employe AS e ON p.fk_proprietaire = e.num_badge;

CREATE VIEW vDirecteurs AS
SELECT s.sigle, s.nom AS nom_site, e.num_badge, e.nom, e.prenom
FROM Site_Industriel AS s
INNER JOIN employe AS e ON s.fk_directeur = e.num_badge;

