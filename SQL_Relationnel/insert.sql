INSERT INTO Batiment (nom_bat, superficie, localisation, nombre_etages) VALUES
    ('A001', 383, '-91.346, 156.457', 3),
    ('A002', 567, '54.657, 109.358', 4),
    ('B001', 239, '-110.136, 140.547', 2),
    ('B002', 115, '87.658, -10.245', 1);

INSERT INTO Plans_Batiment (id_plan, fk_batiment, etage, chemin_plan) VALUES
    (1, 'A001', 1, '/batiments/A001/plans/plan_1.pdf'),
    (2, 'A001', 2, '/batiments/A001/plans/plan_2.pdf'),
    (3, 'A001', 3, '/batiments/A001/plans/plan_3.pdf'),
    (4, 'A002', 1, '/batiments/A002/plans/plan_1.pdf'),
    (5, 'A002', 2, '/batiments/A002/plans/plan_2.pdf'),
    (6, 'A002', 3, '/batiments/A002/plans/plan_3.pdf'),
    (7, 'A002', 4, '/batiments/A002/plans/plan_4.pdf'),
    (8, 'B001', 1, '/batiments/B001/plans/plan_1.pdf'),
    (9, 'B001', 2, '/batiments/B001/plans/plan_2.pdf'),
    (10, 'B002', 1, '/batiments/B002/plans/plan_1.pdf');

INSERT INTO Salle (nom_salle, fk_batiment, superficie, capacite_max, etage, chemin_photos, chemin_plan, caracteristiques_tec) VALUES
    ('A_X1', 'A001', 15, 5, 2, '/salles/A_X1/photos', '/salles/A_X1/plans', (TRUE, TRUE, 'butane', 10, 10)),
    ('A_X2', 'A001', 20, 10, 2, '/salles/A_X2/photos', '/salles/A_X2/plans', (FALSE, TRUE, NULL, 15, 10)),
    ('A_X3', 'A001', 30, 20, 3, '/salles/A_X3/photos', '/salles/A_X3/plans', (TRUE, FALSE, 'azote', 40, 30)),
    ('A_X4', 'A002', 9, 3, 2, '/salles/A_X4/photos', '/salles/A_X4/plans', (FALSE, FALSE, NULL, 15, 10)),
    ('A_X5', 'A002', 14, 5, 2, '/salles/A_X5/photos', '/salles/A_X5/plans', (FALSE, FALSE, 'methane', 10, 20)),
    ('A_X6', 'A002', 20, 6, 2, '/salles/A_X6/photos', '/salles/A_X6/plans', (TRUE, FALSE, 'azote', 25, 40)),
    ('A_X7', 'A002', 60, 20, 3, '/salles/A_X7/photos', '/salles/A_X7/plans', (TRUE, TRUE, 'butane', 20, 30)),
    ('A_X8', 'A002', 60, 19, 3, '/salles/A_X8/photos', '/salles/A_X8/plans', (FALSE, TRUE, 'o2', 30, 30)),
    ('A_X9', 'A002', 90, 25, 4, '/salles/A_X9/photos', '/salles/A_X9/plans', (TRUE, TRUE, 'o2', 50, 25)),
    ('B_X1', 'B001', 20, 10, 2, '/salles/B_X1/photos', '/salles/B_X1/plans', (FALSE, TRUE, NULL, 15, 10)),
    ('B_X2', 'B001', 30, 20, 2, '/salles/B_X2/photos', '/salles/B_X2/plans', (TRUE, FALSE, 'azote', 40, 30));

INSERT INTO Site_Industriel (sigle, nom) VALUES
    ('D_X001', 'Site Alpha'),
    ('D_X002', 'Site Bravo'),
    ('D_X003', 'Site Charlie'),
    ('L_X001', 'Site Delta'),
    ('L_X002', 'Site Echo'),
    ('L_X003', 'Site Foxtrot');
   
INSERT INTO Departement (sigle_dep, domain) VALUES
    ('D_X001', 'Microbiologie'),
    ('D_X002', 'Manipulation genetique'),
    ('D_X003', 'Explosives');

INSERT INTO Laboratoire (sigle_lab, chemin_logo, thematique) VALUES
    ('L_X001', '/sites_industriels/laboratoire/L_X001/logo.png', 'Armes chimiques'),
    ('L_X002', '/sites_industriels/laboratoire/L_X002/logo.png', 'Sante'),
    ('L_X003', '/sites_industriels/laboratoire/L_X003/logo.png', 'Extraction miniere');

INSERT INTO Employe (num_badge, fk_salle_bureau, fk_site, nom, prenom, email, statut) VALUES
    (1001, 'B_X1', 'D_X001', 'Lenoir', 'Alice', 'alice.lenoir@umbrella-corp.fr', 'CDI'),
    (1002, 'B_X1', 'D_X002', 'Leblanc', 'Albert', 'albert.leblanc@umbrella-corp.fr', 'CDI'),
    (1003, 'B_X1', 'D_X003', 'Lerouge', 'Jacques', 'jacques.lerouge@umbrella-corp.fr', 'CDI'),
    (1004, 'B_X2', 'L_X001', 'Lebleu', 'Zoe', 'zoe.lebleu@umbrella-corp.fr', 'CDI'),
    (1005, 'B_X2', 'L_X002', 'Levert', 'Marcel', 'marcel.levert@umbrella-corp.fr', 'CDI'),
    (1006, 'B_X2', 'L_X003', 'Lerose', 'Leon', 'leon.lerose@umbrella-corp.fr', 'CDI'),
    (2010, NULL, 'D_X001', 'Robert', 'Jean', 'jean.robert@umbrella-corp.fr', 'CDD'),
    (2011, NULL, 'D_X001', 'Bruan', 'Axel', 'axel.bruan@umbrella-corp.fr', 'CDD'),
    (2012, NULL, 'L_X001', 'Billot', 'Pauline', 'pauline.billot@umbrella-corp.fr', 'CDD'),
    (2013, NULL, 'L_X001', 'Fredo', 'Paul', 'paul.fredo@umbrella-corp.fr', 'CDD'),
    (9001, NULL, NULL, 'Durant', 'Pierre', 'pierre.durant@umbrella-corp.fr', 'stagiaire'),
    (9002, NULL, NULL, 'Duval', 'Louis', 'louis.duval@umbrella-corp.fr', 'stagiaire');

UPDATE Site_Industriel
SET fk_directeur=1001
WHERE sigle='D_X001';

UPDATE Site_Industriel
SET fk_directeur=1002
WHERE sigle='D_X002';

UPDATE Site_Industriel
SET fk_directeur=1003
WHERE sigle='D_X003';

UPDATE Site_Industriel
SET fk_directeur=1004
WHERE sigle='L_X001';

UPDATE Site_Industriel
SET fk_directeur=1005
WHERE sigle='L_X002';

UPDATE Site_Industriel
SET fk_directeur=1006
WHERE sigle='L_X003';

INSERT INTO Poste_tel (num_int, fk_salle, fk_proprietaire, num_ext, type, modele, marque) VALUES
    (501, 'A_X1', 2010, 5540501, 'VOIP', 'ARX01', 'Motorola'),
    (502, 'A_X1', 2011, 5540502, 'VOIP', 'ARX02', 'Motorola'),
    (9001, 'A_X9', 2012, 55409001, 'TOIP', NULL, NULL),
    (9002, 'A_X9', 2013, 55409002, 'landline', NULL, 'Cisco');

INSERT INTO Projet (sigle_proj, fk_site, nom, start_date, end_date, description) VALUES
    ('PR01', 'D_X001', 'Recherche du mais', '2018-09-28', '2020-01-10', 'Un labour de 25 à 30 cm suivi par un recroisement effectué avec un cultivateur pour travailler en profondeur et aérer la terre.'),
    ('PR02', 'D_X001', 'Vaccination', '2018-09-28', NULL, ' Les vaccins, qui stimulent le système immunitaire, prémunissent la personne d’une infection ou d’une maladie.'),
    ('PR03', 'L_X001', 'Bombe a hydrogene', '2020-05-28', NULL, NULL);

INSERT INTO Projet_Employe (id, fk_projet, fk_employe, role, commentaires) VALUES
    (101, 'PR01', 1001, 'Responsable', 'Directeur du Site Alpha.'),
    (102, 'PR01', 9001, 'Cultivateur', 'Stagiaire en developpement.'),
    (103, 'PR01', 9002, 'Ingenieur en software', 'Stagiaire en developpement.'),
    (104, 'PR02', 1002, 'Responsable', 'Directeur du Site Bravo.'),
    (105, 'PR02', 2010, 'Medecin', NULL),
    (106, 'PR02', 2011, 'Laboratoriste', NULL),
    (107, 'PR02', 2012, 'Laboratoriste', NULL),
    (108, 'PR02', 2013, 'Avocat', 'Pendant le proces du 07/01/2020'),
    (109, 'PR03', 1003, 'Responsable', 'Directeur du Site Charlie.'),
    (110, 'PR03', 2010, 'Chimique', NULL);

INSERT INTO Machine (serie_machine, fk_salle, modele, description, puissance_elec, besoin_tri, besoin_res, num_maintenance, entreprise_maintenance, besoin_gaz, taille, machine_fabrication, machine_laboratoire) VALUES
    ('XEW9012', 'A_X4', 'SDT-ULTRA', 'Microscope', 150, FALSE, FALSE, 983741, 'Bayer', NULL, 'petite', FALSE, TRUE),
    ('AB001', 'A_X4', 'Xtreme', 'Centrifugeuse', 700, TRUE, FALSE, 44818452, 'Schneider', NULL, 'mediane', FALSE, TRUE),
    ('XXX-12381', 'A_X5', 'Hadrone', 'Grand collisionneur de hadrons', 75000, TRUE, TRUE, 401851, 'Inside', 'o2', 'grande', FALSE, TRUE);

INSERT INTO Moyen_Informatique (serie_moyen, fk_machine_liee, fk_employe_resp, nom_moyen, OS, mac_wifi, mac_ethernet, serveur, pc, portable) VALUES
    ('EKQR0012', 'XEW9012', 1001, 'Super PC 1', 'Windows 10', NULL, '00:1e:c2:9e:28:6b', FALSE, TRUE, FALSE),
    ('EKQR00120', 'AB001', 1001, 'Super PC 2', 'Linux', NULL, NULL, TRUE, FALSE, FALSE),
    ('EKQR00122', 'AB001', 1001, 'Portable HP', 'Linux', '12:1f:c2:00:8e:6a', '00:1f:c2:5b:8e:6a', FALSE, FALSE, TRUE),
    ('EWE721', 'XXX-12381', 1004, 'Hyper serveur', 'Linux', '12:1f:c2:00:8e:6a', '00:1f:c2:5b:8e:6a', TRUE, FALSE, FALSE),
    ('CBWN1515', 'XXX-12381', 1005, 'Portable test', 'Mac OS', NULL, '10:1d:d1:5b:9c:6a', FALSE, FALSE, TRUE);

INSERT INTO Moyen_Projet (id, fk_moyen, fk_projet, commentaires) VALUES
    (301, 'EKQR0012', 'PR01', 'Analise des molecules du mais'),
    (302, 'EKQR0012', 'PR02', 'Analise des cellules'),
    (303, 'EKQR00120', 'PR02', NULL),
    (304, 'EKQR00122', 'PR02', NULL),
    (305, 'EKQR0012', 'PR03', 'Analise des molecules de la bombe'),
    (306, 'EWE721', 'PR03', 'Traitement des donnees du collisionneur'),
    (307, 'CBWN1515', 'PR03', NULL);
